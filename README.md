# TUNIC Entrance Tracker

To use this, click the link below to open the tracker in your browser.

https://scipiowright.gitlab.io/tunic-tracker/

Alternatively, download the source code zip and open the file called "index.html" with your browser.

If you refresh your page, your data will be lost. If you're doing a longer session or need to close your browser, please use the save and load buttons in the options menu (the gear icon) to keep your entrance data tracked.

Here is a link to the original, which this is forked off of:
https://sekii.gitlab.io/pokemon-tracker


## License
The code is under the [MIT License](code/LICENSE.txt). Original code by Sekii.

Data, fonts and images belong to their own copyright holders.
