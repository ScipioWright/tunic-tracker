// Text to show as tooltips. It's divided by ~folders~ not by games
// null      = won't get a tooltip
// undefined = default name
const tooltips = {

    /* tunc */
    // Overworld
    ow_wg_stairs_west: null,
    ow_wg_stairs_east: null,
    hourglass_cave: "To Hourglass Cave",
    beach_furnace: "Beach to\nFurnace",
    ruined_shop: null,
    fountain_hc: null,
    town_portal: null,
    house_door: null,
    windmill: null,
    windmill_to_furnace: "Windmill to\nFurnace",
    atoll_upper: "Atoll Upper\nEntrance",
    atoll_lower: "Atoll Lower\nEntrance",
    swamp_upper_entry: "Swamp Upper\nEntrance",
    swamp_lower_entry: "Swamp Lower\nEntrance",
    ruined_hall_not_door: null,
    ruined_hall_door: null,
    temple_door: null,
    stairs_to_mountain: null,
    guard_patrol: null,
    special_shop: null,
    forest_entry: null,
    cube_cave: null,
    quarry_entry: null,
    darktomb_entry: null,
    fairy_cave: null,
    temple_rafters: null,
    fortress_entry: null,
    house_back: "To Back of\nHouse",
    spawn_portal: null,
    se_hc: null,
    changing_room: null,
    wg_laurels_entry: null,
    wg_upper_entry: null,
    well_entry: null,
    stick_house: null,
    rotating_lights: "To Rotating\nLights",
    maze_cave: "To Maze Cave",
    well_rail_n: "Well Rail North",
    well_rail_s: "Well Rail South",
    purgatory_top: null,
    purgatory_bot: null,
    shop1: "Shop",
    shop2: "Shop",
    shop3: "Shop",
    shop4: "Shop",
    shop5: "Shop",
    shop6: "Shop",

    // Overworld Interiors
    int_stick_house: "Stick House Exit",
    int_rotating_lights: "Rotating Lights\nExit",
    int_maze_cave: "Maze Cave Exit",
    int_ruined_hall_not_door: "Ruined Passage\nNot-Door Exit",
    int_ruined_hall_door: "Ruined Passage\nDoor Exit",
    int_fountain_hc: "Fountain Holy\nCross Room",
    int_hourglass_cave: "Hourglass Cave\nExit",
    int_temple_door: "Temple Door Exit",
    int_temple_rafters: "Temple Rafters\nExit",
    int_special_shop: "Special Shop\nExit",
    int_fairy_cave: "Secret Gathering\nPlace Exit",
    int_guard_patrol: "Patrol Cave Exit",
    int_changing_room: "Changing Room\nExit",
    int_cube_cave: "Cube Cave Exit",
    int_se_hc: "SE Cross Room\nExit",
    int_house_door: null,
    int_house_portal: null,
    int_house_back: null,
    int_windmill_front: null,
    int_windmill_back: null,
    int_furnace_to_wg: null,
    int_furnace_to_beach: null,
    int_furance_to_tomb: null,
    int_furnace_to_windmill: null,
    int_furnace_to_well: null,
    int_glyph_tower: null,
    int_ruined_shop: "Ruined Shop Exit",

    // East Forest
    bell_to_ow: null,
    bell_to_boss: "Bell to Boss Room",
    bell_to_forest: "Bell to Forest",
    bell_to_fortress: null,
    gh1_to_boss: null,
    gh1_to_forest_upper: null,
    gh1_to_dance_fox: null,
    gh1_to_forest_lower: null,
    gh2_upper: null,
    gh2_lower: null,
    grave_path_upper: "Grave Path Upper\nExit",
    grave_path_lower: "Grave Path Lower\nExit",
    grave_path_grave: null,
    forest_to_gh2_upper: null,
    forest_to_belltower: null,
    forest_gh1_lower: null,
    forest_gh1_upper: null,
    boss_front: null,
    boss_back: null,
    forest_grave_lower: "Grave Path Lower\nEntrance",
    forest_dance_fox: null,
    forest_to_gh2_lower: null,
    forest_grave_upper: "Grave Path Upper\nEntrance",
    forest_portal: null,

    // Dark Tomb
    dt_to_ow: null,
    dt_to_checkpoint: null,
    dt_to_furnace: null,

    // Beneath the Well
    well_to_furnace: "To Well Rail",
    well_to_checkpoint: "To Checkpoint",
    boss_to_well: null,
    dt_checkpoint: null,
    well_to_ow: null,

    // West Garden
    wg_upper_exit: null,
    wg_lower_exit: null,
    wg_laurels_exit: null,
    grave: null,
    wg_shop: null,
    dagger_entry: null,
    garden_portal: null,
    dagger_exit: null,

    // Beneath the Earth
    bte_to_interior: null,
    bte_to_exterior: null,

    // Fortress
    fortress_from_overworld: null,
    fortress_from_forest: null,
    fortress_main_entry: null,
    fortress_exterior_from_interior: "To Fortress East",
    fortress_west_upper: "To Grave Path\nUpper",
    fortress_shop: null,
    fortress_to_bte: null,
    fortress_west_lower: "To Grave Path\nLower",
    fortress_to_dusty: null,
    fortress_grave: null,
    fortress_grave_path_upper: "Grave Path\nUpper Exit",
    fortress_grave_path_lower: "Grave Path\nLower Exit",
    fortress_east_int_to_exterior: "To Fortress\nCourtyard Upper",
    fortress_east_int_upper: "To Fortress\nInterior Upper",
    fortress_east_int_lower: "To Fortress\nInterior Lower",
    fortress_int_from_bte: null,
    fortress_arena_entry: null,
    fortress_int_shop: null,
    fortres_gold_door: null,
    fortress_int_east_upper: "To Fortress East\nUpper",
    fortress_int_east_lower: "To Fortress East\nLower",
    dusty_exit: null,
    spidertank_portal: null,
    spidertank_exit: null,

    // Atoll
    atoll_portal: "Portal Pad",
    atoll_upper_exit: "Upper Exit",
    atoll_lower_exit: "Lower Exit",
    atoll_shop: null,
    atoll_frog_mouth: null,
    atoll_frog_eye: null,
    atoll_statue: null,

    // Frog's Domain
    frog_eye: null,
    frog_mouth: null,
    frog_ladder_top: null,
    frog_back_entry: null,
    frog_ladder_bot: null,
    frog_exit: null,

    // Library
    library_tree: null,
    library_exterior_ladder: null,
    hall_from_exterior: null,
    library_grave: null,
    hall_ladder: null,
    rotunda_lower: null,
    rotunda_upper: null,
    lab_lower: null,
    lab_upper: null,
    lab_portal: null,
    librarian: null,

    // Quarry
    monastery_entry: null,
    quarry_to_mountain: null,
    quarry_to_connector: null,
    quarry_portal: null,
    quarry_shop: null,
    quarry_to_zig: "To Rooted Ziggurat",
    monastery_side_entry: null,
    connector_to_ow: null,
    connector_to_quarry: null,
    monastery_front_exit: null,
    monastery_grave: null,
    monastery_back_exit: null,
    mountaintop: null,
    mountain_stairs: null,
    mountain_to_quarry: null,
    mountain_to_ow: null,

    // Swamp
    swamp_upper: "Swamp Upper Exit",
    swamp_lower: "Swamp Lower Exit",
    cath_door: null,
    swamp_hc_door: "To Treasure Room",
    swamp_grave: null,
    swamp_shop: null,
    gauntlet_entry: "To Gauntlet",

    // Cathedral
    cath_shop: null,
    gauntlet_exit: null,
    cath_elev_top: null,
    cath_elev_bot: null,
    cath_hc_door: null,
    cath_entry: null,

    // Ziggurat
    zig0_front: null,
    zig0_back: null,
    zig1_elev: "The Big Elevator",
    zig1_back: null,
    zig2_top: null,
    zig2_bot: null,
    zig3_front: null,
    zig_skip_exit: "Zig Skip Exit",
    zig3_back: null,
    zig_portal_door: null,
    zig_portal_pad: null,

    // Far Shore
    heir: null,
    quarry: null,
    west_garden: null,
    atoll: null,
    library: null,
    town: null,
    vault: null,
    zig: null,
    spawn: null,
    forest: null,
    library_grave: null,
    garden_grave: null,
    monastery_grave: null,
    fortress_grave: null,
    forest_grave: null,
    swamp_grave: null,
    heir_exit: "From Heir Arena",
    

    /* General marks */
    unknown:  "Unknown",
    corridor: "Unknown\nCorridor",
    dead_end: "Dead End",
    event:     undefined,
    center:   "Pokémon\nCenter",
    mart:     "Pokémon\nMart",
    one_way:   undefined,
    // Types left undefined
    1: null, 2: null, 3: null, 4: null,
    5: null, 6: null, 7: null, 8: null,
    "#ce4069": "Red",  "#ff9c54": "Orange", "#f3d23b": "Yellow", "#743683": "Purple", "#654321": "Brown", "#014f01": "Dark Green",
    "#4d90d5": "Blue", "#74cec0": "Cyan",   "#90c12c": "Green",  "#ec8fe6": "Pink",   "#5a5366": "Gray",  "#00146b": "Dark Blue",

    masterball: "Master Ball",

    /* Crystal */
    team_rocket:  "Team\nRocket",
    c_rival:      "Silver\n(Rival)",
    c_gyarados:   "Red\nGyarados",
    c_legendary:  "Lugia\nHo-oh",
    burned_tower: "Burned\nTower",
    c_kurt:       "Kurt's\nHouse",
    c_unown:      "Unown\nRoom",
    c_train:      "Magnet\nTrain",
    dratini:      "Dragon's\nDen Room",
    c_bush:       "Blocked by\nCut",
    c_surf:       "Blocked by\nSurf",
    c_boulder:    "Blocked by\nStrength",
    c_darkness:   "Blocked by\nFlash",
    c_whirlpool:  "Blocked by\nWhirlpool",
    c_waterfall:  "Blocked by\nWaterfall",
    c_rock:       "Blocked by\nRock Smash",
    bike_needed:  "Blocked by\nBicycle Req.",
    c_trainer:    "Blocked by\nTrainer Fight",
    c_daycare:    "Pokémon\nDay Care",

    zephyr_badge:  "Zephyr Badge\n(Flash)",
    hive_badge:    "Hive Badge\n(Cut)",
    plain_badge:   "Plain Badge\n(Strength)",
    fog_badge:     "Fog Badge\n(Surf)",
    storm_badge:   "Storm Badge\n(Fly)",
    mineral_badge: undefined,
    glacier_badge: "Glacier Badge\n(Whirlpool)",
    rising_badge:  "Rising Badge\n(Waterfall)",
    c_will:  "Elite 4\nWill",
    c_koga:  "Elite 4\nKoga",
    c_bruno: "Elite 4\nBruno",
    c_karen: "Elite 4\nKaren",
    c_lance: "Champion\nLance",
    c_oak:   "Professor\nOak's Lab",
    c_red:   "Red\n(Post-Game)",
    hm_cut:       "HM 01\n(Cut)",
    hm_fly:       "HM 02\n(Fly)",
    hm_surf:      "HM 03\n(Surf)",
    hm_strength:  "HM 04\n(Strength)",
    hm_flash:     "HM 05\n(Flash)",
    hm_whirlpool: "HM 06\n(Whirlpool)",
    hm_waterfall: "HM 07\n(Waterfall)",
    crystal: {
        hm_rocksmash: "TM 08\n(Rock Smash)",
    },
    pokegear:       "Pokégear",
    radio_card:     undefined,
    expansion_card: undefined,
    squirt_bottle:  undefined,
    secret_potion:  undefined,
    card_key:       "Radio Tower\nCard Key",
    ss_ticket:      "S.S. Anne\nTicket",
    pass:           "Magnet Train\nPass",
    machine_part:   undefined,
    clear_bell:     undefined,
    rainbow_wing:   undefined,
    silver_wing:    undefined,
    basement_key:   undefined,
    lost_item:      undefined,
    red_scale:      undefined,
    mystery_egg:    undefined,
    pokedex:        "Pokédex",
    bike:           "Bicycle",
    blue_card:      "Buena's\nBlue Card",
    coin_case:      undefined,
    itemfinder:     undefined,
    old_rod:        undefined,
    good_rod:       undefined,
    super_rod:      undefined,

    /* Emerald */
    e_trainer:    "Blocked by\nTrainer Fight",
    e_legendary:  "Kyogre\nGroudon",
    e_bush:       "Blocked by\nCut",
    e_surf:       "Blocked by\nSurf",
    e_boulder:    "Blocked by\nStrength",
    e_darkness:   "Blocked by\nFlash",
    e_rock:       "Blocked by\nRock Smash",
    e_waterfall:  "Blocked by\nWaterfall",
    e_dive:       "Blocked by\nDive",

    stone_badge:   "Stone Badge\n(Cut)",
    knuckle_badge: "Knuckle Badge\n(Flash)",
    dynamo_badge:  "Dynamo Badge\n(Rock Smash)",
    heat_badge:    "Heat Badge\n(Strength)",
    balance_badge: "Balance Badge\n(Surf)",
    feather_badge: "Feather Badge\n(Fly)",
    mind_badge:    "Mind Badge\n(Dive)",
    rain_badge:    "Rain Badge\n(Waterfall)",
    e_sidney:  "Elite 4\nSidney",
    e_phoebe:  "Elite 4\nPhoebe",
    e_glacia:  "Elite 4\nGlacia",
    e_drake:   "Elite 4\nDrake",
    e_wallace: "Champion\nWallace",
    e_steven:  "Steven\n(Post-Game)",
    storage_key: "Abandoned Ship\nStorage Key",
    magma_emblem: undefined,
    go_goggles:  "Go-Goggles",
    devon_scope:  undefined,
    hm_rocksmash: "HM 06\n(Rock Smash)",
    hm_dive:      "HM 08\n(Dive)",

    /* Fire Red - Leaf Green */
    frlg_rival: "Blue\n(Rival)",
    safari:     "Safari Zone",
    frlg_trainer:   "Blocked by\nTrainer Fight",
    frlg_bush:      "Blocked by\nCut",
    frlg_surf:      "Blocked by\nSurf",
    frlg_boulder:   "Blocked by\nStrength",
    frlg_darkness:  "Blocked by\nFlash",
    frlg_rock:      "Blocked by\nRock Smash",
    frlg_waterfall: "Blocked by\nWaterfall",
    frlg: {
        masterball: "Top Floor\nSilph Co.",
        boulder_badge: "Boulder Badge\n(Flash)",
        cascade_badge: "Cascade Badge\n(Cut)",
        thunder_badge: "Thunder Badge\n(Fly)",
        rainbow_badge: "Rainbow Badge\n(Strength)",
        soul_badge:    "Soul Badge\n(Surf)",
        marsh_badge:   "Marsh Badge\n(Rock Smash)",
        volcano_badge: "Volcano Badge\n(Waterfall)",
        earth_badge:    undefined,
        card_key:   "Silph Co.\nCard Key",
        lift_key:   "Rocket's Hideout\nLift Key",
        secret_key: "Blaine's Gym\nSecret Key",
    },

    frlg_lorelei: "Elite 4\nLorelei",
    frlg_bruno:   "Elite 4\nBruno",
    frlg_agatha:  "Elite 4\nAgatha",
    frlg_lance:   "Elite 4\nLance",
    frlg_blue:    "Champion\nBlue",
    parcel: "Oak's Parcel",
    poke_flute: "PokéFlute",
    tea: undefined,
    silph_scope: undefined,
    gold_teeth: undefined,
    tri_pass: "Tri-Pass",

    /* Platinum */
    regis: "Regice Regirock\nRegisteel",
    p_legendary: "Palkia Dialga",
    p_legendary2: "Giratina Arceus\nDarkrai",
    p_guardians: "Uxie Azelf\nMesprit",
    p_trainer:   "Blocked by\nTrainer Fight",
    p_bush:      "Blocked by\nCut",
    p_surf:      "Blocked by\nSurf",
    p_boulder:   "Blocked by\nStrength",
    p_fog:       "Blocked by\nDefog",
    p_rock:      "Blocked by\nRock Smash",
    p_waterfall: "Blocked by\nWaterfall",
    p_rockywall: "Blocked by\nRock Climb",

    coal_badge:   "Coal Badge\n(Rock Smash)",
    forest_badge: "Forest Badge\n(Cut)",
    cobble_badge: "Cobble Badge\n(Fly)",
    fen_badge:    "Fen Badge\n(Surf)",
    relic_badge:  "Relic Badge\n(Defog)",
    mine_badge:   "Mine Badge\n(Strength)",
    icicle_badge: "Icile Badge\n(Rock Climb)",
    beacon_badge: "Beacon Badge\n(Waterfall)",
    p_aaron:   "Elite 4\nAaron",
    p_bertha:  "Elite 4\nBertha",
    p_flint:   "Elite 4\nFlint",
    p_lucian:  "Elite 4\nLucian",
    p_cynthia: "Champion\nCynthia",
    hm_defog: "HM 05\n(Defog)",
    hm_rockclimb: "HM 08\n(Rock Climb)",

    /* Heart Gold - Soul Silver */
    embedded_tower: "Embedded\nTower",
    hgss_surf:       "Blocked by\nSurf",
    hgss_whirlpool:  "Blocked by\nWhirlpool",
    hgss_darkness:   "Blocked by\nFlash",
    hgss: {
        zephyr_badge:  "Zephyr Badge\n(Rock Smash)",
        glacier_badge: "Glacier Badge\n(Whirlpool)",
        rising_badge:  "Rising Badge\n(Waterfall)",
        earth_badge:   "Earth Badge\n(Rock Climb)",
        hm_whirlpool:  "HM 05\n(Whirlpool)",
        hm_rocksmash:  "HM 06\n(Rock Smash)",
        hm_flash:      "TM 70\n(Flash)",
    },
    hgss_will:  "Elite 4\nWill",
    hgss_koga:  "Elite 4\nKoga",
    hgss_bruno: "Elite 4\nBruno",
    hgss_karen: "Elite 4\nKaren",
    hgss_lance: "Champion\nLance",
    hgss_red:   "Red\n(Post-Game)",

    /* Black 2 - White 2 */
    w2_n: "N",
    w2_ghetsis: "Ghetsis",
    b2w2_legendary: "Kyurem\nReshiram Zekrom",
    w2_bush:       "Blocked by\nCut",
    w2_surf:       "Blocked by\nSurf",
    w2_boulder:    "Blocked by\nStrength",
    w2_waterfall:  "Blocked by\nWaterfall",
    basicbadge:  "Basic Badge",
    toxicbadge:  "Toxic Badge",
    insectbadge: "Insect Badge",
    boltbadge:   "Bolt Badge",
    quakebadge:  "Quake Badge",
    jetbadge:    "Jet Badge",
    legendbadge: "Legend Badge",
    wavebadge:   "Wave Badge",

    black2white2: {
        hm_waterfall: "HM 05\n(Waterfall)",
        hm_dive: "HM 06\n(Dive)",
    },

    bw_shauntal:  "Elite 4\nShauntal",
    bw_marshal:   "Elite 4\nMarshal",
    bw_grimsley:  "Elite 4\nGrimsley",
    bw_caitlin:   "Elite 4\nCaitlin",
    b2w2_iris:    "Champion\nIris",
    bw_N:         "N\n(Post-Game)",
    bw_alder:     "Alder\n(Post-Game)",
    bw_cynthia:   "Cynthia\n(Post-Game)",

    white2_a: {
        shauntal: "Elite 4\nShauntal",
        marshal:  "Elite 4\nMarshal",
        grimsley: "Elite 4\nGrimsley",
        catlin:   "Elite 4\nCaitlin",
        champ:    "Champion\nIris",
    },

    /* Item stuff */
    item_overworld: "Item found with \nno requirement",
    item_checked:   "Item checked\nand not useful",
    item_event:        "Key Item / Event\nRequired",
    item_surf:         "Surf\nRequired",
    item_cut:          "Cut\nRequired",
    item_strength:     "Strength\nRequired",
    item_rocksmash:    "Rock Smash\nRequired",
    item_whirlpool:    "Whirlpool\nRequired",
    item_basement_key: "Basement Key\nRequired",
    item_phone:        "Phone Call\nRequired",
}